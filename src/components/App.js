import React, { Component } from 'react';
import './App.css';
import DateInput from './DateInput';
import TimeUntil from './TimeUntil';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timeLeft: null,
      interval: null
    }
    this.getDate = this.getDate.bind(this);
  }

  getDate(newDate, currentDate) {

    this.state.interval && clearInterval(this.state.interval);

    const timeLeft = newDate - currentDate;
    this.setState({ timeLeft });

    const countdown = setInterval(() => {
      this.setState((prevState) => {
        return { timeLeft: prevState.timeLeft - 1 };
      });
    }, 1000);

    this.setState({ interval: countdown });
  }

  render() {
    return (
      <div className="App">
        <DateInput getDate={this.getDate} />
        <TimeUntil timeLeft={this.state.timeLeft} />
      </div>
    );
  }
}

export default App;
