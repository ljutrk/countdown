import React from 'react';

const DateInput = ({getDate}) => {

    function onInputChange(e) {
        const currentDate = Math.floor(new Date().getTime() / 1000);
        const newDate = Math.floor(new Date(e.target.value).getTime() / 1000);
        getDate(newDate - 7200, currentDate); // -7200 because GMT +2 
    }

    return (
        <input onChange={onInputChange} type="date" />
    );
}

export default DateInput;