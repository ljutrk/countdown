import React from 'react';

const TimeUntil = ({ timeLeft }) => {
    if (timeLeft === null) {
        return <p>No Date Chosen!</p>
    }

    if (timeLeft < 0) {
        return <p>Please select date in future!</p>
    }

    const formatDate = (seconds) => {
        let days = Math.floor(seconds / 86400);
        seconds = seconds % 86400
        let hours = Math.floor(seconds / 3600);
        seconds = seconds % 3600
        let minutes = Math.floor(seconds / 60);
        seconds = seconds % 60
        return `Days: ${days} Hours: ${hours} Minutes: ${minutes} Seconds: ${seconds}`
    }

    return (
        <div>
            <br />
            <p>Total seconds until selected day: {timeLeft} </p>
            <br />
            <p>Time until date: {formatDate(timeLeft)}</p>
        </div>
    );
}

export default TimeUntil;